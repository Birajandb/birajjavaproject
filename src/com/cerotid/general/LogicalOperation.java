package com.cerotid.general;

public class LogicalOperation {

	public static void main(String[] args) {
		// Variables Definition and Initialization
		boolean bool1 = true, bool2 = false;
		
		//Logical And
		System.out.println("bool1 && bool2 = " + (bool1 && bool2));
		
		//Logical Or
		System.out.println("bool1 || bool2=" + (bool1 || bool2));
		
		//Logical Not
		System.out.println("!(bool1 && bool2) = " + !(bool1 && bool2));
		
	
		
	}

}
