package com.cerotid.general;

public class BitwiseOperators {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1 = 30, num2 = 6, num3 = 0;
		
		//Bitwise And
		System.out.println("num1 & num2 = " + (num1 & num2));
		
		//Bitwise Or
		System.out.println("num1 | num2 = " + (num1 | num2));

		//Bitwise XOR
				System.out.println("num1 ^ num2 = " + (num1 ^ num2));
				
				//Binary Complement Operator
				System.out.println("~ num1 = " + ~num1 );
				
				//Binary Left Shift Operator
				num3 = num1 << 2;
				System.out.println("num1 << 1 = " + num3);
				
				//Binary right Shift Operator
				num3 = num1 >> 2;
				System.out.println("num1 >> 1 = " + num3);
				
				//Shift right zero fill operator
				num3 = num1 >>> 2;
				System.out.println("num1 >>> 1 = " + num3);
				
				
				
			


	}

}
 