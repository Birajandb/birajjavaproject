package com.cerotid.general;

public class LoopConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("My name is Biraj");
		System.out.println("My name is Biraj");
		System.out.println("My name is Biraj");
		
		//As above, this is not the way to print many name, we use LoopConcept to make easy
		//Types of Loop i.e. 
		//For Loop
		//While Loop
		//Do While Loop
		//Foreach
		
		//For Loop using variable,
		for (int i = 0; i < 5; i++) {
			System.out.println("My last name is Bidari");
			
		}
		
		//For While Loop
		int j = 0;
		while (j < 5) {
			System.out.println("I am in Java Class");
			j++;
			
			System.out.println("Value of j = " + j);
		}
		
		//Do While Loop
		int k =0;
		do { System.out.println("I am in path to be a programmer");
		k = k+1; //or we can write k++;
		} while (k < 5);
		
		
		
		
		
		
		
		//System.out.println("*");
		//System.out.println("**");
		//System.out.println("***");
		//System.out.println("****");
		//System.out.println("*****");
		
		//Types of Loop
		//For Loop
		//While Loop
		//Do while loop
		//Foreach
		
		//For Loop using Variable
		//for (int i = 0; i < 5; i++) {System.out.println("My Name is Biraj");
			
		
		
		//int j = 0;
		
		//while (j < 5) {
			System.out.println("I am in java class");
			
			//j++: // j = j+1;
		
				

	}

}
